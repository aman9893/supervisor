import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-superviser-details-more-info',
  templateUrl: './superviser-details-more-info.component.html',
  styleUrls: ['./superviser-details-more-info.component.css']
})
export class SuperviserDetailsMoreInfoComponent implements OnInit {
  constructor( private back_location: Location,) { }

  ngOnInit() {
  }

  goBack(): void {
    this.back_location.back();
  }
}
