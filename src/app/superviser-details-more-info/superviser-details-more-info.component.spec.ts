import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperviserDetailsMoreInfoComponent } from './superviser-details-more-info.component';

describe('SuperviserDetailsMoreInfoComponent', () => {
  let component: SuperviserDetailsMoreInfoComponent;
  let fixture: ComponentFixture<SuperviserDetailsMoreInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperviserDetailsMoreInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperviserDetailsMoreInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
