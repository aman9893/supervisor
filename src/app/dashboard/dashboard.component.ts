import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import { SuperviserDialogComponent } from '../superviser-dialog/superviser-dialog.component';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

 
  constructor(public dialog: MatDialog, public snackBar: MatSnackBar) {}

  ngOnInit() {
  
  }

  openSuper(){
    this.addBlog()
  }

  addBlog() {
    let updatedata = {
      data: 'aman',
      flag: 'true'
    }

    const dialogRef = this.dialog.open(SuperviserDialogComponent, {
      width: '450px',
      data: updatedata,
      panelClass:'superviserDialog',
      autoFocus:false
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

}
