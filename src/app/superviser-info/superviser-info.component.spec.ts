import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperviserInfoComponent } from './superviser-info.component';

describe('SuperviserInfoComponent', () => {
  let component: SuperviserInfoComponent;
  let fixture: ComponentFixture<SuperviserInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperviserInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperviserInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
