import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import { SuperviserDialogComponent } from '../superviser-dialog/superviser-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-superviser-info',
  templateUrl: './superviser-info.component.html',
  styleUrls: ['./superviser-info.component.css']
})
export class SuperviserInfoComponent implements OnInit {

  
  constructor( private back_location: Location,) { }

  ngOnInit() {
  }

  goBack(): void {
    this.back_location.back();
  }
}
