import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperviserDetailsInfoComponent } from './superviser-details-info.component';

describe('SuperviserDetailsInfoComponent', () => {
  let component: SuperviserDetailsInfoComponent;
  let fixture: ComponentFixture<SuperviserDetailsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperviserDetailsInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperviserDetailsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
