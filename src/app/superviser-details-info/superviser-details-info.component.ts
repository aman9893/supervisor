import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
@Component({
  selector: 'app-superviser-details-info',
  templateUrl: './superviser-details-info.component.html',
  styleUrls: ['./superviser-details-info.component.css']
})
export class SuperviserDetailsInfoComponent implements OnInit {

  constructor( private back_location: Location,) { }

  ngOnInit() {
  
      var fauxTable = document.getElementById("faux-table");
      var mainTable = document.getElementById("main-table");
      var clonedElement = mainTable.cloneNode(true);
      var clonedElement2 = mainTable.cloneNode(true);
     
      fauxTable.appendChild(clonedElement);
      fauxTable.appendChild(clonedElement2);
    
  }

  goBack(): void {
    this.back_location.back();
  }
}
