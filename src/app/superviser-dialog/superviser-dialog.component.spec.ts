import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperviserDialogComponent } from './superviser-dialog.component';

describe('SuperviserDialogComponent', () => {
  let component: SuperviserDialogComponent;
  let fixture: ComponentFixture<SuperviserDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperviserDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperviserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
