import { Component, OnInit ,Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent, MatAutocompleteModule, MatAutocompleteTrigger } from '@angular/material';
import { Router } from '@angular/router';
@Component({
  selector: 'app-superviser-dialog',
  templateUrl: './superviser-dialog.component.html',
  styleUrls: ['./superviser-dialog.component.css']
})
export class SuperviserDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SuperviserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public update_data: any, private router:Router) {
    }

    Superviser= [
    'Pending Supervisor Decision', 
    'Approved By Central Materials', 
    'Pending For Revalidation'
  ];


  ngOnInit() {
  }

  cancel(){
    this.dialogRef.close();
  }

  submit(){

    this.router.navigate(['/superviserInfo']);
    this.dialogRef.close();
  }

}
