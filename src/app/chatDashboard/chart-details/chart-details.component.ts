import { Component, OnInit ,Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent, MatAutocompleteModule, MatAutocompleteTrigger } from '@angular/material';
import { Router } from '@angular/router';
@Component({
  selector: 'app-chart-details',
  templateUrl: './chart-details.component.html',
  styleUrls: ['./chart-details.component.css']
})
export class ChartDetailsComponent implements OnInit {
constructor(
  public dialogRef: MatDialogRef<ChartDetailsComponent>,
  @Inject(MAT_DIALOG_DATA) public update_data: any, private router:Router) {
  }

  ngOnInit() {
  console.log(this.update_data.data)
  
  }
  close(){
    this.dialogRef.close();
  }
}
