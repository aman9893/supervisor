import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart6',
  templateUrl: './chart6.component.html',
  styleUrls: ['./chart6.component.css']
})
export class Chart6Component  {
 // Pie
 public pieChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
 public pieChartData:number[] = [300, 500, 100];
 public pieChartType:string = 'pie';

 // events
 public chartClicked(e:any):void {
   console.log(e);
 }

 public chartHovered(e:any):void {
   console.log(e);
 }
}
