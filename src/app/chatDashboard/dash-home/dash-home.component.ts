import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { Chart1Component } from '../chart1/chart1.component';
import { ChartDetailsComponent } from '../chart-details/chart-details.component';

@Component({
  selector: 'app-dash-home',
  templateUrl: './dash-home.component.html',
  styleUrls: ['./dash-home.component.css']
})
export class DashHomeComponent implements OnInit {
  updatedata: { data: string; };

  constructor(public dialog: MatDialog, public snackBar: MatSnackBar) { }

  ngOnInit() {

  }


  chartone(datas) {
    console.log(datas)
    if (datas === 'one') {
      this.updatedata = {
        data: 'one'
      }
    }
    if (datas === 'two') {
      this.updatedata = {
        data: 'two'
      }
    }
    if (datas === 'three') {
      this.updatedata = {
        data: 'three'
      }
    }
    const dialogRef = this.dialog.open(ChartDetailsComponent, {
      width: '1000px',
      data: this.updatedata,
      panelClass: 'superviserDialog',
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

}
