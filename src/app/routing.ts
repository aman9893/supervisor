import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SuperviserInfoComponent } from './superviser-info/superviser-info.component';
import { SuperviserDetailsInfoComponent } from './superviser-details-info/superviser-details-info.component';
import { SuperviserDetailsMoreInfoComponent } from './superviser-details-more-info/superviser-details-more-info.component';
import { DashHomeComponent } from './chatDashboard/dash-home/dash-home.component';

export const  AppRoutes: Routes = [
    { path: '', component: DashboardComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'chartjs', component: DashHomeComponent },
    { path: 'superviserInfo', component: SuperviserInfoComponent },
    { path: 'superviserDetailsInfo', component: SuperviserDetailsInfoComponent },
    { path: 'supervisermoreDetailsInfo', component: SuperviserDetailsMoreInfoComponent },
    ]


export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
