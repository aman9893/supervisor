import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ROUTING } from './routing';
import { AppComponent } from './app.component';
import { MaterialModule } from './material';
import { ChartsModule } from 'ng2-charts';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SuperviserDialogComponent } from './superviser-dialog/superviser-dialog.component';
import { SuperviserInfoComponent } from './superviser-info/superviser-info.component';
import { SuperviserDetailsInfoComponent } from './superviser-details-info/superviser-details-info.component';
import { SuperviserDetailsMoreInfoComponent } from './superviser-details-more-info/superviser-details-more-info.component';
import { HeaderComponent } from './header/header.component';
import { ConfrimDialogComponent } from './confrim-dialog/confrim-dialog.component';
import { DashHomeComponent } from './chatDashboard/dash-home/dash-home.component';
import { DashDetailsComponent } from './chatDashboard/dash-details/dash-details.component';
import { Chart1Component } from './chatDashboard/chart1/chart1.component';
import { Chart2Component } from './chatDashboard/chart2/chart2.component';
import { Chart3Component } from './chatDashboard/chart3/chart3.component';
import { Chart4Component } from './chatDashboard/chart4/chart4.component';
import { Chart5Component } from './chatDashboard/chart5/chart5.component';
import { Chart6Component } from './chatDashboard/chart6/chart6.component';
import { ChartDetailsComponent } from './chatDashboard/chart-details/chart-details.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SuperviserDialogComponent,
    SuperviserInfoComponent,
    SuperviserDetailsInfoComponent,
    SuperviserDetailsMoreInfoComponent,
    HeaderComponent,
    ConfrimDialogComponent,
    DashHomeComponent,
    DashDetailsComponent,
    Chart1Component,
    Chart2Component,
    Chart3Component,
    Chart4Component,
    Chart5Component,
    Chart6Component,
    ChartDetailsComponent
  ],
  imports: [MaterialModule,ROUTING,RouterModule,
    BrowserModule,BrowserAnimationsModule,ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[SuperviserDialogComponent,ConfrimDialogComponent,
    ChartDetailsComponent,Chart1Component]
})
export class AppModule { }
